<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    
    public function index()
    {
        try {

            $data = Employee::with("role")->get();
            $response['data'] = $data;
            $response['success'] = true;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['success'] = false;
        }
        return $response;
    }

    
    public function store(Request $request)
    {
        try {
  
          $insert['fname'] = $request['fname'];
          $insert['lname'] = $request['lname'];
          $insert['email'] = $request['email'];
          $insert['city'] = $request['city'];
          $insert['direction'] = $request['address'];
          $insert['phone'] = $request['phone'];
          $insert['rol'] = $request['rol'];
  
          Employee::insert($insert);
  
          $response['message'] = "บันทึกข้อมูลสำเร็จ";
          $response['succes'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['succes'] = true;
        }
         
        return $response;
    }

    
    public function show($id)
    {
        try {
  
          $data = Employee::with("role")->find($id);
  
          if ($data) {
            $response['data'] = $data;
            $response['message'] = "Load successful";
            $response['success'] = true;
          }
          else {
            $response['message'] = "ไม่พบข้อมูลรายการ id => $id";
            $response['success'] = false;
          }
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
    }

    public function update(Request $request,$id){

        try {
  
          $data['fname'] = $request['fname'];
          $data['lname'] = $request['lname'];
          $data['email'] = $request['email'];
          $data['city'] = $request['city'];
          $data['direction'] = $request['address'];
          $data['phone'] = $request['phone'];
          $data['rol'] = $request['rol'];
  
          Employee::where("id",$id)->update($data);
  
          $response['message'] = "แก้ไขข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
  
    }

    
    public function destroy($id)
    {
        try {
          $res = Employee::where("id",$id)->delete();
          $response['res'] = $res;
          $response['message'] = "ลบข้อมูลสำเร็จ";
          $response['success'] = true; 
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
  
        return $response;
    }
}
